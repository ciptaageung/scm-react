import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'lavender'
  }
});

export const loginStyles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'lavender',
    alignItems: 'center',
    flexGrow: 1,
    justifyContent: 'center',
    padding:20
  },
  logo: {
    position: 'relative',
    width: 300,
    height: 200,
    marginBottom:50
  },
  input:{
    height: 40,
    backgroundColor: 'rgba(255,255,255,0.2)',
    marginBottom: 10,
    padding: 10,
    color: '#777',
    width:300,
    borderRadius:2
  },
  buttonContainer:{
    backgroundColor: '#e91e63',
    paddingVertical: 15,
    width:300,
    borderRadius:2
  },
  buttonText:{
    color: '#fff',
    textAlign: 'center',
    fontWeight: '700'
  },
  text:{
    color:'#e91e63',
    fontWeight:'600',
    fontSize:24
  }
});

export default styles;