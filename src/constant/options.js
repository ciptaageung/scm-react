import React from 'react';
import Icon from 'react-native-vector-icons/Ionicons';

export const navigationStyle = {
  headerStyle: {
    backgroundColor: '#e91e63'
  },
  tabBarOptions: {
    activeTintColor: '#e91e63',
    inactiveTintColor: 'gray',
  }
}

export const header = ({ navigation }) => ({
  headerTitle: navigation.state.routeName,
  headerLeft : <Icon onPress={() => navigation.openDrawer()} name='ios-keypad' size={25} color='#ffffff' />,
  headerTitleStyle: {
    color: '#fefefe',
    fontWeight: '300',
    textAlign:'center',     
    flex: 1,
    marginHorizontal: 0
  },
  headerTitleContainerStyle:{ left:0,position:'absolute'},
  headerTintColor: '#fefefe',
  headerLeftContainerStyle:{marginLeft:10},
  headerStyle: navigationStyle.headerStyle
});

export function getTabBarIcon(state, focused) {
  const { routeName } = state;
  if (routeName === 'Siswa') {
    return `user${focused ? '' : ''}`;
  } else if (routeName === 'Guru') {
    return `user-secret${focused ? '' : ''}`;
  } else if (routeName === 'Home') {
    return `home${focused ? '' : ''}`;
  } else if (routeName === 'Profile') {
    return `info${focused ? '' : ''}`;
  }
}