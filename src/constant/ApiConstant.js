export const apiConstants = {
    POST: {
        method: 'POST',
        headers: { 'Content-Type': 'application/json;charset=UTF-8'},
        credentials: 'include'
    },
    PUT: {
        method: 'PUT',
        headers: { 'Content-Type': 'application/json;charset=UTF-8'},
        credentials: 'include'
    },
    GET: {
        method: 'GET',
        headers: { 
            'Authorization': 'Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImF1dGgiOiJST0xFX0FETUlOLFJPTEVfVVNFUiIsImV4cCI6MTU1Njg2NjMzOX0.dGE38L2A97HqmkA6n9ji_rv0I5HAG09W9fVaTCzLxf_Q0D2NmUDl_Zdf-ARnv5EDaEVijWQKi-TeVxpmnxkcqQ',
            'Content-Type': 'application/json;charset=UTF-8'
        },
        credentials: 'include'
    },
    DELETE: {
        method: 'DELETE',
        headers: { 'Content-Type': 'application/json;charset=UTF-8'},
        credentials: 'include'
    }
};

export function apiConstantsBody(method, body) {
    console.log(Object.assign(apiConstants.POST, body));
    if(method==='POST'){
        return Object.assign(apiConstants.POST, body);
    }else if(method==='PUT'){
        return Object.assign(apiConstants.PUT, body);
    }else if(method==='DELETE'){
        return Object.assign(apiConstants.DELETE, body);
    }else{
        return Object.assign(apiConstants.GET, body);
    }
}

export function apiConstantsFetch(name) {
    return { 
        success : 'FETCH_'+name.toUpperCase()+'_SUCCESS',
        failure : 'FETCH_'+name.toUpperCase()+'_FAILURE',
        begin : 'FETCH_'+name.toUpperCase()+'_BEGIN',
    }
}