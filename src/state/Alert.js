export const RESPONSE = {
  SUCCESS: { messages:'Success Process', color: 'GREEN', hexCode: '#17A05E' },
  FAILED: { messages:'Failed Process', color: 'RED', hexCode: '#DE5448' }
}