import { apiConstants, apiConstantsBody, apiConstantsFetch } from '../constant/ApiConstant';
import { axiosInstance } from '../helpers/ApiInterceptor';

export var reduceName = "";
export var fetchApiName = {};

export const fetchApiBegin = (actionData) => ({
    type : fetchApiName.begin,
    payload : { actionData }
});

export const fetchApiSuccess = (data, actionData) => ({
    type : fetchApiName.success,
    payload : { data, actionData }
})

export const fetchApiFailure = (error, actionData) => ({
    type : fetchApiName.failure,
    payload : { error, actionData }
})

export function fetchApiAction (modeAction){
    alert(modeAction.actionName);
    reduceName = modeAction.actionName;
    fetchApiName = apiConstantsFetch(reduceName);
    return dispatch =>{
        dispatch(fetchApiBegin(reduceName));
        axiosInstance.get('/account')
            .then(response => {
                dispatch(fetchApiSuccess(response.data, reduceName))
            })
            .catch(error => {
                if(error.response){
                    console.log(error.response.data);
                    console.log(error.response.status);
                    console.log(error.response.headers);
                }else if(error.request) {
                    console.log(error.request);
                }else{
                    console.log('Error', error.message);
                }
                console.log(error.config);
                dispatch(fetchApiFailure(error, reduceName));
            });
    }
}