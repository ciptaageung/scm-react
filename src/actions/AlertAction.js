export const responseChanged = (type) => {
    return {
      type: 'RESPONSE_CHANGED',
      payload: type
    };
};