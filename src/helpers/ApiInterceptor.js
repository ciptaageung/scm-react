import axios from 'axios';
import { AsyncStorage } from 'react-native';

export const axiosInstance = axios.create({
    baseURL: 'http://emart.elevenia.co.id/api/',
    timeout: 10000,
    headers: {
        'Content-Type': 'application/json;charset=UTF-8'
    },
    credentials: 'include'
});

// Add a request interceptor
axiosInstance.interceptors.request.use(function (config) {
    // Do something before request is sent
    const userToken = AsyncStorage.getAllKeys();
    expect(userToken).toContain('userToken');
    alert(userToken);
    if ( userToken != null ) {
        config.headers.Authorization = `Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImF1dGgiOiJST0xFX0FETUlOLFJPTEVfVVNFUiIsImV4cCI6MTU1Njk1NTAyOX0.Ryp5sxRpTDJboBcrgmrYLUNmDea2QC92My01i_pnD7jfT6v2pbH0ony0CcooUf4OTw3tozufi3AmehUW-jHTvA`;
    }
    return config;
  }, function (error) {
    // Do something with request error
    return Promise.reject(error);
  });

// Add a response interceptor
axiosInstance.interceptors.response.use(function (response) {
    // Do something with response data
    return response;
  }, function (error) {
    // Do something with response error
    return Promise.reject(error);
  });