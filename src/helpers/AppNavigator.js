import React from 'react';
import { createDrawerNavigator, createAppContainer, createStackNavigator, createSwitchNavigator } from 'react-navigation';
import { connect } from "react-redux";
import Icon from 'react-native-vector-icons/FontAwesome';

import LoginScreen from '../screen/login/LoginScreen';
import RegisterScreen from '../screen/register/RegisterScreen';
import AuthLoadingScreen from './AuthLoadingScreen';
import mainTabs from './MainNavigator';
import dashboardTabs from './DasboardNavigator';

const AuthStack = createStackNavigator(
  { 
    Login: { screen: LoginScreen } , 
    Register: { screen: RegisterScreen }
  }
);

const DrawerContent = (props) => (
  <View>
    <View
      style={{
        backgroundColor: '#f50057',
        height: 140,
        alignItems: 'center',
        justifyContent: 'center',
      }}
    >
      <Text style={{ color: 'white', fontSize: 30 }}>
        Header
      </Text>
    </View>
    <DrawerItems {...props} />
  </View>
)

const appDrawer = createDrawerNavigator(
  {
    Beranda: { screen: mainTabs, navigationOptions: {
      drawerIcon: ({ tintColor }) => (
        <Icon name="home" size={24} style={{ color: 'black' }} />
      )
    }},
    Monitoring: { screen: dashboardTabs , navigationOptions: {
      drawerIcon: ({ tintColor }) => (
        <Icon name="desktop" size={24} style={{ color: 'black' }} />
      )
    }}
  },
  {
    headerMode: 'screen',
    initialRouteName: 'Beranda',
    contentOptions: {
      activeTintColor: '#e91e63',
      itemsContainerStyle: {
        marginVertical: 0,
      },
      iconContainerStyle: {
        opacity: 1
      }
    },
    drawerWidth: 200
  }
);

export const AppContainer = createAppContainer(createSwitchNavigator(
    {
      AuthLoading: AuthLoadingScreen,
      App: appDrawer,
      Auth: AuthStack,
    },
    {
      initialRouteName: 'AuthLoading',
    }
  ));

  const AppWithNavigationState = ({ dispatch, nav }) => (
    <AppContainer  />
  );

  const mapStateToProps = state => ({
    nav: state.nav,
  });

  export default connect(mapStateToProps)(AppWithNavigationState);