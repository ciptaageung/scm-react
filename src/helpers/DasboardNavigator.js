import React from 'react';

import { createStackNavigator, createBottomTabNavigator } from 'react-navigation';
import Icon from 'react-native-vector-icons/FontAwesome';

import SiswaDashScreen from '../screen/siswa/SiswaDashScreen';
import GuruDashScreen from '../screen/guru/GuruDashScreen';

import {navigationStyle, getTabBarIcon, header} from '../constant/options';

//create siswa stack
const siswaDashStack = createStackNavigator({
    Siswa: { 
      screen: SiswaDashScreen,
      navigationOptions:header
  }
});

//create guru stack
const guruDashStack = createStackNavigator({
    Guru: { 
      screen: GuruDashScreen,
      navigationOptions:header
  }
});

//create screen for dashboard stack
const dashboardTabs = createBottomTabNavigator(
    {
      Siswa: siswaDashStack,
      Guru: guruDashStack,
    },
    {
      defaultNavigationOptions: ({ navigation }) => ({
        tabBarIcon: ({ focused, tintColor }) => {
          return <Icon name={getTabBarIcon(navigation.state, focused)} size={25} color={tintColor} />;
        },
      }),
      tabBarOptions: navigationStyle.tabBarOptions,
    }
) 

export default dashboardTabs;