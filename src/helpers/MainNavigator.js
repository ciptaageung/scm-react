import React from 'react';

import { createStackNavigator, createBottomTabNavigator } from 'react-navigation';
import Icon from 'react-native-vector-icons/FontAwesome';

import HomeScreen from '../screen/home/HomeScreen';
import ProfileScreen from '../screen/profile/ProfileScreen';

import {navigationStyle, getTabBarIcon, header} from '../constant/options';

//create home stack
const homeStack = createStackNavigator({
    Home: { 
      screen: HomeScreen, 
      navigationOptions:header
    }
});

//create Profile stack
const profileStack = createStackNavigator({
    Profile: { 
      screen: ProfileScreen, 
      navigationOptions:header 
    }
});

//create screen for main stack
const mainTabs = createBottomTabNavigator(
    {
      Home: homeStack,
      Profile: profileStack,
    },
    {
      navigationOptions:{
        headerMode: 'float'
      },
      defaultNavigationOptions: ({ navigation }) => ({
        tabBarIcon: ({ focused, tintColor }) => {
          return <Icon name={getTabBarIcon(navigation.state, focused)} size={25} color={tintColor} />;
        },
      }),
      tabBarOptions: navigationStyle.tabBarOptions
    }
)

export default mainTabs;