const initialState = {
  status: 'SUCCESS',
};

const AlertReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'RESPONSE_CHANGED':
      return { ...state, status: action.payload.status };
    default:
      return state;
  }
};

export default AlertReducer;