import { combineReducers } from 'redux';
import AlertReducer from './AlertReducer';
import apiReducer from './ApiReducer';

const AppReducer = combineReducers({
  messages: AlertReducer, apiReducer
});

export default AppReducer;