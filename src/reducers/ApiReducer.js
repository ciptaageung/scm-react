const initialState = {
    items : {},
    loading : false,
    error : null
}
export default function apiReducer(state = initialState, action){

    var reduceName = '';

    if(action.type.includes("FETCH")){

        /** declare type api berdasarkan action data masing masing modul */
        const FETCH_API_SUCCESS = 'FETCH_'+(action.payload.actionData).toUpperCase()+'_SUCCESS';
        const FETCH_API_FAILURE = 'FETCH_'+(action.payload.actionData).toUpperCase()+'_FAILURE';
        const FETCH_API_BEGIN = 'FETCH_'+(action.payload.actionData).toUpperCase()+'_BEGIN';

        switch(action.type){

            case FETCH_API_BEGIN :
                return {
                    ...state,
                    loading : true,
                    error : null
                }

            case FETCH_API_SUCCESS :
                const dataAssign = {};
                dataAssign[ action.payload.actionData] = action.payload.data;
                return {
                    ...state,
                    loading : false,
                    items : dataAssign
                }

            case FETCH_API_FAILURE :
                return {
                    ...state,
                    loading:true,
                    error: action.payload.error,
                    items:null
                }

            default : 
                return state;

        }
    }else{
        return state;
    }
}