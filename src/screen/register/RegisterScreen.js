import React from 'react';
import {
    StyleSheet,
    AsyncStorage,
    View,
    Text,
    TextInput,
    TouchableOpacity,
    Image
} from 'react-native';

export default class RegisterScreen extends React.Component {

  constructor(props) {
		super(props);

		this.state = {
			username: '',
      password: '',
      isLoading: true
    };
    
    this.handleChange = this.handleChange.bind(this);
  }
  
  static navigationOptions = {
    header: null
  };

  handleChange(e) {
		const { name, value } = e.target;
		this.setState({ [name]: value });
	}

  render() {
    const { username, password } = this.state;
    return (
      <View style={styles.container}>
         <Image resizeMode="contain" style={styles.logo} source={require('../../../assets/pdd.png')}  />
            <TextInput style = {styles.input} placeholder="username" onChangeText={(username) => this.setState({username})}/>

            <TextInput style = {styles.input} placeholder="password" onChangeText={(password) => this.setState({password})}/>

            <TouchableOpacity style={styles.buttonContainer} onPress={this._signInAsync} >
              <Text  style={styles.buttonText}>Register</Text>
            </TouchableOpacity> 
      </View>
    );
  }

  _signInAsync = async () => {
    await AsyncStorage.setItem('userToken', 'abc');
    this.props.navigation.navigate('App');
  };
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#2c3e50',
        alignItems: 'center',
        flexGrow: 1,
        justifyContent: 'center',
        padding:20
    },
    logo: {
        position: 'relative',
        width: 300,
        height: 200,
        marginBottom:50
    },

    input:{
        height: 40,
        backgroundColor: 'rgba(225,225,225,0.2)',
        marginBottom: 10,
        padding: 10,
        color: '#fff',
        width:300,
        borderRadius:2
    },
    buttonContainer:{
        backgroundColor: '#2980b6',
        paddingVertical: 15,
        width:300,
        borderRadius:2
    },
    buttonText:{
        color: '#fff',
        textAlign: 'center',
        fontWeight: '700'
    }
  });