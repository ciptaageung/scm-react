import React from 'react';

import {
  AsyncStorage,
  View,
  Text,
  TextInput,
  TouchableOpacity,
  Image
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Input } from 'react-native-elements';
import { loginStyles } from '../../constant/style';

export default class LoginScreen extends React.Component {

  constructor(props) {
    super(props);
    AsyncStorage.clear();
		this.state = {
			username: '',
      password: '',
      isLoading: true
    };
    this.handleChange = this.handleChange.bind(this);
  }

  static navigationOptions = {
    header: null
  };

  handleChange(e) {
		const { name, value } = e.target;
		this.setState({ [name]: value });
  }
  
  render() {
    const { username, password } = this.state;
    return (
      <View style={loginStyles.container}>
          <Image resizeMode="contain" style={loginStyles.logo} source={require('../../../assets/pded.png')}  />
          <Text style = {loginStyles.text}>IPin App</Text>
          <Text>Indonesia Pintar, silahkan login untuk melanjutkan!{"\n\n\n"}</Text>
          
          <Input style = {loginStyles.input} placeholder=' username' leftIcon={ 
            <Icon name='user-secret' size={24} color='gray' />
          } onChangeText={(username) => this.setState({username})}/>

          <Input style = {loginStyles.input} placeholder=' password' leftIcon={ 
            <Icon name='lock' size={24} color='gray' />
          } onChangeText={(password) => this.setState({password})}/>

          <TouchableOpacity style={loginStyles.buttonContainer} onPress={this._signInAsync} >
            <Text  style={loginStyles.buttonText}>LOGIN</Text>
          </TouchableOpacity> 
      </View>
    );
  }

  _signInAsync = async () => {
    await AsyncStorage.setItem('userToken', 'eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImF1dGgiOiJST0xFX0FETUlOLFJPTEVfVVNFUiIsImV4cCI6MTU1Njk1NTAyOX0.Ryp5sxRpTDJboBcrgmrYLUNmDea2QC92My01i_pnD7jfT6v2pbH0ony0CcooUf4OTw3tozufi3AmehUW-jHTvA');
    this.props.navigation.navigate('App');
  };
}