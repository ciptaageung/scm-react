import React, { Component } from 'react';
import { connect } from "react-redux";
import styles from '../../constant/style';
import { RESPONSE } from '../../state/Alert.js';
import {
  AsyncStorage,
  Button,
  View,
  Text
} from 'react-native';

class HomeScreen extends Component {

  onChooseAlert() {
    this.props.navigation.navigate('Profile');
}

  selecteAlert() {
    const { status } = this.props;
    return RESPONSE[status].hexCode;
  }
    render() {
      const status = this.selecteAlert();
        return (
          <View style={{ flex: 1, alignSelf: 'stretch' }}>
                <Button 
                    onPress={this.onChooseAlert.bind(this)}
                    color="#FFF"
                    title="Choose Color"
                />
            </View>
        );
      }
      _signOutAsync = async () => { 
        await AsyncStorage.clear();
        this.props.navigation.navigate('Auth');
      };
    }

    const mapStateToProps = state => {
      return { status: state.messages.status } ;
  };
  
  export default connect(mapStateToProps)(HomeScreen);