import React, { Component } from 'react';
import { connect } from "react-redux";
import {
  Button,
  View,
  Text
} from 'react-native';
import styles from '../../constant/style';
import { responseChanged } from '../../actions/AlertAction.js';
import { RESPONSE } from '../../state/Alert.js';
import { fetchApiAction } from "../../actions/ApiAction";

class ProfileScreen extends Component {

  componentDidMount() {
    var modeAction = {
      urlName : "/account",
      actionName: "profile"
    };
    this.props.dispatch(fetchApiAction(modeAction));
  }

  onSelectAlert(status) { 
    this.props.responseChanged({ status });
    this.props.navigation.goBack();
  }

  render() {
    const { error, loading, profile } = this.props;

    var loginName = '';

    if (error) {
      return <Text>Error! {error.message}</Text>;
    }

    if (loading) {
      return <Text>Loading...</Text>;
    }

    if(profile){
      loginName = profile.login;
    }

    return (

      <View style={styles.container}>
          <Text>This is Profile!</Text>
          <Text>
          {loginName}
          </Text>
        </View>
    );
  }
}

const mapStateToProps = state => ({
  profile: state.apiReducer.items.profile,
  loading: state.apiReducer.loading,
  error: state.apiReducer.error
});

export default connect(mapStateToProps)(ProfileScreen);