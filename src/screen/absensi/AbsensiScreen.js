import React from 'react';
import {
  StyleSheet,
  AsyncStorage,
  Button,
  View,
} from 'react-native';

export default class AbsensiScreen extends React.Component {
    render() {
      return (
        <View style={styles.container}>
          <Button title="This is Absensi" onPress={this._signOutAsync} />
        </View>
      );
    }
  
    _signOutAsync = async () => {
      await AsyncStorage.clear();
      this.props.navigation.navigate('Auth');
    };
  }

  const styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: '#2c3e50'
    },
  });