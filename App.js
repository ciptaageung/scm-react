import React from 'react';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware   } from 'redux';
import thunk from 'redux-thunk';

import AppContainer from './src/helpers/AppNavigator';
import AppReducer from './src/reducers/AppReducer';
import { axiosInstance } from './src/helpers/ApiInterceptor';

const store = createStore(AppReducer,applyMiddleware(thunk));

export default class App extends React.Component {

  render() {
    return (
      <Provider store={store}>
        <AppContainer
          ref={nav => {
           this.navigator = nav;
          }}
        />
      </Provider>
    );
  }
}